package cz.cvut.fel.ts1.lab2;

import cz.cvut.fel.ts1.lab2.Calculator;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    public void setUp() {
        System.out.println("Running setUp");
        calculator = new Calculator();
    }

    @Test
    @DisplayName("Test secti 5 + 10 a dostan 15.")
    @Order(1)
    public void addition_5plus10_11() {
        System.out.println("Running addition_5plus10_11");
        //Arrange
        int x = 5;
        int y = 10;
        int expected = 15;

        // Act
        int result = calculator.add(x,y);

        //Assert
        Assertions.assertEquals(expected,result);
    }

    @Test
    @DisplayName("Test odesecti 10 - 5 a dostan 5.")
    @Order(2)
    public void subtract_10minus5_5() {
        System.out.println("Running subtract_5plus10_11");
        //Arrange
        int x = 10;
        int y = 5;
        int expected = 5;

        // Act
        int result = calculator.subtract(x,y);

        //Assert
        Assertions.assertEquals(expected,result);
    }

    @Test
    @DisplayName("Test nasobi 5 * 10 a dostan 50.")
    @Order(3)
    public void multiply_5times10_50() {
        System.out.println("Running multiply_5times10_50");
        //Arrange
        int x = 5;
        int y = 10;
        int expected = 50;

        // Act
        int result = calculator.multiply(x,y);

        //Assert
        Assertions.assertEquals(expected,result);
    }

    @Test
    @DisplayName("Test rozdeli 10 / 5 a dostan 2.")
    @Order(5)
    public void divide_10dividedBy5_2() {
        System.out.println("Running divide_10dividedBy5_2");
        //Arrange
        int x = 10;
        int y = 5;
        int expected = 2;

        // Act
        int result = calculator.divide(x,y);

        //Assert
        Assertions.assertEquals(expected,result);
    }

    @Test
    @Order(4)
    public void divide_divideByZero_throwsAssertion(){
        System.out.println("Running divide_divideByZero_throwsAssertion");
        //Arrange
        int x = 10;
        int y = 0;

        //Assert
        Assertions.assertThrows(ArithmeticException.class, () -> {
            calculator.divide(x,y);
        });
    }
}
