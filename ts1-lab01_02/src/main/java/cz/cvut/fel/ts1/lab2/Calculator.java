package cz.cvut.fel.ts1.lab2;

public class Calculator {

    public int add(int x, int y) {
        return x + y;
    }

    public int subtract(int x, int y) {
        return x - y;
    }

    public int multiply(int x, int y) {
        return x * y;
    }

    public int divide(int x, int y) {
        if (y == 0) throw new ArithmeticException("Divide by 0");
        return x / y;
    }
}
