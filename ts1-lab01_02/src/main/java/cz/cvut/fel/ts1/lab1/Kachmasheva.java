package cz.cvut.fel.ts1.lab1;

public class Kachmasheva {

    public static void main(String[] args) {
        System.out.println("Factorial is : " + factorial(5));
    }

    public static long factorial(int n) {
        if (n < 0) return 0;

        int result = n;
        for( int i = 0; i < n; i++) {
            result = result * n;
        }
        return result;
    }

}
